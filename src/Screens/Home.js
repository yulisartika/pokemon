import React from "react";
import { Link } from "react-router-dom";

export default function Home() {
  return (
    <div>
      <h1>Home Page</h1>
      <button
        type="button"
        // onClick={() => history.push("/pokemon")} another approach
      >
        <Link to="/pokemon-one">Pokemon One</Link>
      </button>
      <button type="button">
        <Link to="/pokemon-two">Pokemon Two</Link>
      </button>
      <button>
        <Link to="/pokemon-three">Pokemon Three</Link>
      </button>
    </div>
  );
}
