import React from "react";
// import { Navbar, Nav, NavItem, NavbarText } from "reactstrap";
import { NavLink } from "react-router-dom";
// import pokemonImange from "../Images/logo.png";

export default function Navigation() {
  return (
    <div>
      <div className="navigation">
        {/* <div>
          <img src={pokemonImange} className="navigationImage" />
        </div> */}

        <NavLink
          to="/"
          exact={true}
          className="navLink"
          activeClassName="navLinkActive"
        >
          Home
        </NavLink>
        <NavLink
          to="/pokemon-one"
          className="navLink"
          activeClassName="navLinkActive"
        >
          PokemonOne
        </NavLink>
        <NavLink
          to="/pokemon-two"
          className="navLink"
          activeClassName="navLinkActive"
        >
          PokemonTwo
        </NavLink>
        <NavLink
          to="/pokemon-three"
          className="navLink"
          activeClassName="navLinkActive"
        >
          PokemonThree
        </NavLink>
        <NavLink
          to="/pokemon-hooks"
          className="navLink"
          activeClassName="navLinkActive"
        >
          PokemonHooks
        </NavLink>
        <NavLink
          to="/contact-us"
          className="navLink"
          activeClassName="navLinkActive"
        >
          Contact Us
        </NavLink>
      </div>

      {/* <Navbar color="light" light expand="md">
        <Nav className="mr-auto" navbar>
          <NavItem>
            <NavLink to="/" className="navLink">
              <h5>Home</h5>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink to="/pokemon-one" className="navLink">
              <h5>Pokemon One</h5>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink to="/pokemon-two" className="navLink">
              <h5>Pokemon Two</h5>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink to="/pokemon-three" className="navLink">
              <h5>Pokemon Three</h5>
            </NavLink>
          </NavItem>
        </Nav>
        <NavbarText>
          <img src={pokemonImange} width="150px" />
        </NavbarText>
      </Navbar> */}
    </div>
  );
}
