import React, { useState } from "react";

export default function ContactUs() {
  const [name, setName] = useState("");
  const [message, setMessage] = useState("");
  const [developers, setDevelopers] = useState([
    { id: 1, name: "Kyle" },
    { id: 2, name: "Karlo" },
  ]);

  const [newDev, setNewDev] = useState("");

  const handleSubmitDev = (e) => {
    e.preventDefault();
    setDevelopers([
      ...developers,
      {
        id: developers.length + 1,
        name: newDev,
      },
    ]);
    setNewDev("");
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    alert(`hai ${name}! Your message has been sent`);
  };

  return (
    <div>
      <h1>Contact Us</h1>
      <form onSubmit={handleSubmit}>
        <h3>Name: </h3>
        <input
          type="text"
          value={name}
          onChange={({ target: { value } }) => setName(value)}
        />
        <h3>Message: </h3>
        <input
          type="text"
          value={message}
          onChange={({ target: { value } }) => setMessage(value)}
        />
        <button type="submit">Submit</button>
        <div>Name input: {name}</div>
        <div>Message input: {message}</div>
      </form>
      <br />
      <br />
      <h5>Developers</h5>
      <div>
        {developers.map((developer) => {
          return <div>{developer.name}</div>;
        })}
      </div>
      <form onSubmit={handleSubmitDev}>
        <input
          type="text"
          value={newDev}
          onChange={({ target: { value } }) => setNewDev(value)}
        />
        <button type="submit">Add</button>
      </form>
    </div>
  );
}

// import React, { useState } from "react";

// export default function ContactUs() {
//   const [name, setName] = useState("");
//   const [msg, setMsg] = useState("");
//   const [developers, setDevelopers] = useState([
//     { id: 1, name: "budi" },
//     { id: 2, name: "santo" },
//   ]);

//   const [newDev, setNewDev] = useState("");

//   const handleSubmit = (event) => {
//     event.preventDefault();
//     setDevelopers([
//       ...developers,
//       {
//         id: developers.length + 1,
//         name: newDev,
//       },
//     ]);
//     setNewDev(""); // untuk nge set supaya pas udah diklik, text yg diketiknya ilang, nanti coba di submit!!!
//   };

//   return (
//     <div>
//       <h1>Contact Us</h1>
//       <h2>Name: </h2>
//       <input
//         type="text"
//         value={name}
//         onChange={({ target: { value } }) => setName(value)}
//       />
//       <h2>Message: </h2>
//       <input
//         type="text"
//         value={msg}
//         onChange={({ target: { value } }) => setMsg(value)}
//       />
//       <div>Name Input: {name}</div>
//       <div>Message Input: {msg}</div>
//       <br />
//       <br />
//       <h5>Developers</h5>
//       {developers.map((developer) => (
//         <div>{developer.name}</div>
//       ))}
//       <br />
//       <div>Add Developers</div>
//       <form onSubmit={handleSubmit}>
//         <input
//           type="text"
//           value={newDev}
//           onChange={({ target: { value } }) => setNewDev(value)}
//         />
//         <button type="submit">Tambah</button>
//       </form>
//     </div>
//   );
// }
