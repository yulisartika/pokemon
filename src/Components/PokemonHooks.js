import React, { useEffect, useState } from "react";
import axios from "axios";

export default function PokemonHooks(props) {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);

  // fetchind data using useEffect
  useEffect(() => {
    axios
      .get("https://pokeapi.co/api/v2/pokemon/?limit=100")
      .then((response) => {
        setData(response.data.results);
        setLoading(false);
      });

    console.log("Mounted!");

    return () => {
      console.log("Unmounted!");
    };
  }, []);

  const getDetails = (id) => {
    console.log(id);
    props.history.push(`/pokemon-hooks/${id}`);
  };

  return (
    <div id="pokemon">
      <div className="pokemon-container">
        {loading ? (
          <div>Loading...</div>
        ) : (
          data.map((item, index) => {
            return (
              <div key={index} className="pokemon-item">
                <img
                  src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/${
                    index + 1
                  }.png`}
                  alt={item.name}
                />
                <h2>{item.name}</h2>
                <button onClick={() => getDetails(index + 1)}>
                  Know Me More!
                </button>
              </div>
            );
          })
        )}
      </div>
    </div>
  );
}

// -------------------------FIRST CODING---------------------------------
// import React, { useState, useEffect } from "react";
// import axios from "axios";
// import loadingLogo from "../Images/logo.gif";

// export default function PokemonHooks(props) {
//   const [data, setData] = useState([]);
//   const [loading, setLoading] = useState(true);

//   // componentDidMount
//   useEffect(() => {
//     axios
//       .get("https://pokeapi.co/api/v2/pokemon/?limit=100")
//       .then((response) => {
//         setData(response.data.results);
//         setLoading(false);
//       });
//   }, []);

//   const getDetails = (id) => {
//     console.log(id);
//     props.history.push(`/pokemon-hooks/${id}`);
//   };

//   return (
//     <div id="pokemon">
//       <div className="pokemon-container">
//         {loading ? (
//           <img src={loadingLogo} alt="loading logo" className="loadingImage" />
//         ) : (
//           data.map((item, index) => (
//             <div key={index} className="pokemon-item">
//               {
//                 <div>
//                   <img
//                     src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${
//                       index + 1
//                     }.png`}
//                     alt={item.name}
//                   />
//                   <h2>{item.name}</h2>
//                   <button onClick={() => getDetails(index + 1)}>Details</button>
//                 </div>
//               }
//             </div>
//           ))
//         )}
//       </div>
//     </div>
//   );
// }

// --------------------------BORDER------------------------------------

// -------EXERCISE BELLOW-------
// import React, { useState, useEffect } from "react";
// import axios from "axios";

// export default function PokemonHooks() {
//   // parameter1: state, parameter2: method untuk mengubah state || 1 fungsi/method itu untuk mengubah 1 state
//   //   const [data, setData] = useState([]); // didalam useState adalah nilai default dari data
//   const [pokemonName, setPokemonName] = useState("Pokemon");
//   //   const [pokemonImage, setPokemonImage] = useState("2");

//   // harus punya 2 parameter, useEffect bkrja spr didMount, hanya akan berjalan di awal dan tidak akan berjalan lagi
//   // componentDidMount
//   useEffect(() => {
//     // kalau mau nge fetch, taruh di sini / componentDidMount, jgn lupa [] untuk pembatasnya
//     // alert("Mounted!");
//   }, []); // [] ketika ada perubahan state disini, nnti fungsi akan jalan

//   // componentDidUpdate -> akan berjalan terus selama ada perubahan state => hati2 fetching disini, infinite loop is waiting there
//   //   useEffect(() => {
//   //     alert("Updated!");
//   //   }); // ketika tdk ada [], maka behaviournya akan spt componentDidUpdate yang behaviournya akan berjalan terus selama ada perubahan state

//   //   useEffect(() => {
//   //     alert("Name Changed!");
//   //   }, [pokemonName, pokemonImage]);

//   // componentWillUnmount
//   //   useEffect(() => {
//   //     return () => {
//   //       alert("Unmounted");
//   //     };
//   //   }, [pokemonName]);

//   return (
//     <div>
//       <div id="pokemon">
//         <div className="pokemon-container">
//           <div className="pokemon-item">
//             <img
//               src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/2.png`}
//               alt=""
//             />
//             <h2>{pokemonName}</h2>
//             <button onClick={() => setPokemonName("Joko")}>Change Name</button>
//             {/* <button onClick={() => setPokemonImage("2")}>Change Image</button> */}
//           </div>
//         </div>
//       </div>
//     </div>
//   );
// }
