import React, { useState, useEffect } from "react";

import axios from "axios";

export default function PokemonHooksDetails(props) {
  const [data, setData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const pokemonId = props.match.params.id;

    axios
      .get(`https://pokeapi.co/api/v2/pokemon/${pokemonId}`)
      .then((response) => {
        setData(response.data);
        console.log(response.data);
        setIsLoading(false);
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <div id="pokemon-details">
      <div className="pokemon-container">
        {isLoading ? (
          <div>Loading...</div>
        ) : (
          data !== null && (
            <div className="pokemon-item">
              <img src={data.sprites.front_shiny} alt={data.name} />
              <h2>{data.name}</h2>
            </div>
          )
        )}
      </div>
    </div>
  );
}

// import axios from "axios";
// import React, { useState, useEffect } from "react";

// export default function PokemonHooksDetails(props) {
//   const [data, setData] = useState(null);

//   useEffect(() => {
//     const pokemonId = props.match.params.id;

//     axios
//       .get(`https://pokeapi.co/api/v2/pokemon/${pokemonId}`)
//       .then((response) => {
//         setData(response.data);
//         console.log(response.data);
//       })
//       .catch((err) => console.log(err));
//   }, []);

//   return (
//     <div id="pokemon-details">
//       <div className="pokemon-container">
//         {data !== null && (
//           <div className="pokemon-item">
//             <img src={data.sprites.front_default} alt={data.name} />
//             <h2>{data.name}</h2>
//           </div>
//         )}
//       </div>
//     </div>
//   );
// }
