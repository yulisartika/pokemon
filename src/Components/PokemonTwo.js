import axios from "axios";
import React, { Component, Fragment } from "react";
import { Col, Row } from "reactstrap";
import PokemonCard from "./PokemonCard";
// import Modal from "react-modal";

export default class PokemonTwo extends Component {
  constructor() {
    super();
    this.state = {
      pokemon: [],
      limit: 12,
      offset: 5,
      selectedPokemonForModal: null,
    };
  }

  // here, fetching data with axios will be used for practice purposes
  componentDidMount() {
    this.fetchDataWithAxios();
  }

  // fetching data using fetch()
  fetchDataWithFetch = () => {
    const { limit, offset } = this.state;
    fetch(`https://pokeapi.co/api/v2/pokemon?limit=${limit}&offset${offset}`)
      .then((response) => response.json())
      .then((data) => this.setState({ pokemon: data.results }))
      .catch((error) => console.log(error));
  };

  // fetching data using axios
  fetchDataWithAxios = () => {
    const { limit, offset } = this.state;
    axios
      .get(`https://pokeapi.co/api/v2/pokemon?limit=${limit}&offset${offset}`)
      .then((response) => this.setState({ pokemon: response.data.results }))
      .catch((error) => console.log(error));
  };

  // fetching data using async await
  fetchDataWithAsync = async () => {
    const { limit, offset } = this.state;
    try {
      const response = await axios.get(
        `https://pokeapi.co/api/v2/pokemon?limit=${limit}&offset${offset}`
      );
      this.setState({ pokemon: response.data.results });
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    const { pokemon, offset } = this.state;
    const eachPokemon =
      pokemon.length === 0
        ? null
        : pokemon.map((item, index) => (
            <Col
              xs="6"
              sm="3"
              md="2"
              className="p-6 column-two"
              key={index}
              onClick={() =>
                this.setState({ selectedPokemonForModal: index + 1 }, () =>
                  alert("hallo")
                )
              }
            >
              <PokemonCard
                image={`https://pokeres.bastionbot.org/images/pokemon/${
                  offset + index + 1 // data yang ditampilkan dimulai dari index ke 5 krn offset nya = 5 di set di atas
                }.png`}
                name={item.name}
                content={item.url}
                index={item.index}
              />
            </Col>
          ));
    return (
      <Fragment>
        <button onClick={() => this.props.history.goBack()}>Back</button>
        <div className="card-wrapper">
          <Row>{eachPokemon}</Row>
        </div>
        {/* <Modal
          isOpen={this.state.selectedPokemonForModal}
          onRequestClose={() =>
            this.setState({ selectedPokemonForModal: null })
          }
          contentLabel="Modal for detail pokemon"
        >
          <h1>pokemon ID-nya adalah : {this.state.selectedPokemonForModal}</h1>
          <button
            className="border-solid border-4 border-light-blue-500 rounded-xl p-2"
            onClick={() => this.setState({ selectedPokemonForModal: null })}
          >
            Close Modal
          </button>
        </Modal> */}
      </Fragment>
    );
  }
}
