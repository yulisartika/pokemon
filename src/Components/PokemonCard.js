import { Card, Button, CardImg, CardTitle, CardBody } from "reactstrap";
import React from "react";

export default function PokemonCard({ name, image, content }) {
  const handleClick = (event) => {
    event.preventDefault();
    alert(`You just clicked ${name}!`);
  };

  return (
    <div>
      <Card className="mt-4 mb-4">
        <CardImg
          top
          width="100%"
          src={image}
          alt="Card image cap"
          className="mt-2 p-3 imageSize"
        />
        <CardBody className="text-center">
          <CardTitle tag="h5">{name}</CardTitle>
          {/* <CardSubtitle tag="h6" className="mb-2 text-muted">
            {subtitle}
          </CardSubtitle> */}
          <div>
            <p style={{ display: content ? content : "none" }}>
              <b
                style={{
                  background: "#cfdac8",
                  padding: "6px",
                  borderRadius: "5px",
                }}
              >
                Url:
              </b>{" "}
              {content}
            </p>
          </div>

          <Button type="submit" onClick={handleClick}>
            Pick Me
          </Button>
        </CardBody>
      </Card>
    </div>
  );
}
