import React, { Component } from "react";

export default class PokemonOneDetail extends Component {
  render() {
    return (
      <div>
        <h1>ID-nya: {this.props.match.params.id}</h1>
        <button onClick={() => this.props.history.goBack()}>Back</button>
      </div>
    );
  }
}
