import React, { Component, Fragment } from "react";
import { Col, Row } from "reactstrap";
import PokemonCard from "./PokemonCard";
import axios from "axios";

export default class PokemonOne extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      isLoading: true,
    };
  }

  componentDidMount() {
    axios
      .get("https://pokeapi.co/api/v2/pokemon/?limit=100")
      .then((response) => {
        this.setState({
          data: response.data.results,
          isLoading: false,
        });
      })
      .catch((error) => console.log(error));
  }

  // componentDidMount() {
  //   this.setState({ isLoading: true });
  //   fetch("https://pokeapi.co/api/v2/pokemon/?limit=12")
  //     .then((response) => response.json())
  //     .then((data) => this.setState({ data: data.results, isLoading: false }));
  // }

  render() {
    const { data } = this.state;
    // const text = isLoading ? "Loading..." : data;

    const eachPokemon =
      data.length === 0
        ? null
        : data.map((item, index) => (
            <Col
              xs="6"
              sm="3"
              md="2"
              className="p-6 column-one"
              key={index}
              onClick={() =>
                this.props.history.push(`/pokemon-one/${index + 1}`)
              }
              // onClick={() =>
              //   this.setState({ selectedPokemonForModal: index + 1 })
              // }
            >
              <PokemonCard
                name={item.name}
                image={`https://pokeres.bastionbot.org/images/pokemon/${
                  index + 1
                }.png`}
              />
            </Col>
          ));

    return (
      <Fragment>
        <button onClick={() => this.props.history.goBack()}>Back</button>
        <div className="card-wrapper">
          {this.state.isLoading ? (
            <div> Loading... </div>
          ) : (
            <Row>{eachPokemon}</Row>
          )}
          {/* <p>{text}</p> */}
          {/* <Row>{eachPokemon}</Row> */}
        </div>
      </Fragment>
    );
  }
}
