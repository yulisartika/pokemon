import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Navigation from "./Components/Navigation";
import PokemonHooks from "./Components/PokemonHooks";
import PokemonHooksDetails from "./Components/PokemonHooksDetails";
import PokemonOne from "./Components/PokemonOne";
import PokemonOneDetail from "./Components/PokemonOneDetail";
import PokemonThree from "./Components/PokemonThree";
import PokemonTwo from "./Components/PokemonTwo";
import Home from "./Screens/Home";
import ContactUs from "./Components/ContactUs";

export default function App() {
  return (
    <Router>
      <Navigation />

      <Switch>
        <Route component={Home} path="/" exact={true} />
        <Route component={PokemonOneDetail} path="/pokemon-one/:id" />
        <Route component={PokemonOne} path="/pokemon-one" />
        <Route component={PokemonTwo} path="/pokemon-two" />
        <Route component={PokemonThree} path="/pokemon-three" />
        <Route component={PokemonHooks} path="/pokemon-hooks" exact={true} />
        <Route component={PokemonHooksDetails} path="/pokemon-hooks/:id" />
        <Route component={ContactUs} path="/contact-us" />
      </Switch>
    </Router>
  );
}
